package nl.bernts.wordcount;

import nl.bernts.wordcount.wordcountlib.WordFrequencyAnalyzer;
import nl.bernts.wordcount.wordcountlib.WordFrequencyAnalyzerImpl;
import org.junit.Assert;
import org.junit.Test;

public class WordFrequencyAnalyzerImplCalcFreqForWordTests {
    private WordFrequencyAnalyzer instance = new WordFrequencyAnalyzerImpl();
    private String inputText;
    private String inputWord;
    private int expectedFrequency;

    @Test
    public void testIncorrectWordFormatEmpty() {
        inputText = "";
        inputWord = "";

        CatchException();
    }

    @Test
    public void testIncorrectWordFormatComma() {
        inputText = "";
        inputWord = "Fo,o";

        CatchException();
    }

    private void CatchException() {
        try {
            instance.calculateFrequencyForWord(inputText, inputWord);
            Assert.fail("Exception should be thrown.");
        } catch (Exception e) {
            // passed
        }
    }

    @Test
    public void testEmptyText() {
        inputText = "";
        inputWord = "foo";
        expectedFrequency = 0;

        testResult();
    }

    @Test
    public void testWordNotInText() {
        inputText = "bar";
        inputWord = "foo";
        expectedFrequency = 0;

        testResult();
    }

    @Test
    public void testWordInText() {
        inputText = "foo";
        inputWord = "foo";
        expectedFrequency = 1;

        testResult();
    }

    @Test
    public void testWordInTextTwice() {
        inputText = "foo foo";
        inputWord = "foo";
        expectedFrequency = 2;

        testResult();
    }

    @Test
    public void testWordInTextTwiceCases() {
        inputText = "Foo fOo";
        inputWord = "foO";
        expectedFrequency = 2;

        testResult();
    }

    @Test
    public void testWordInTextTwiceCommas() {
        inputText = "foo,foo";
        inputWord = "foo";
        expectedFrequency = 2;

        testResult();
    }

    @Test
    public void testWordInTextWithOtherWord() {
        inputText = "foo bar";
        inputWord = "foo";
        expectedFrequency = 1;

        testResult();
    }

    @Test
    public void testWordInTextWithOtherWordCommas() {
        inputText = "foo,bar";
        inputWord = "foo";
        expectedFrequency = 1;

        testResult();
    }

    private void testResult() {
        int output = instance.calculateFrequencyForWord(inputText, inputWord);
        Assert.assertEquals(expectedFrequency, output);
    }
}
