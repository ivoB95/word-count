package nl.bernts.wordcount;

import nl.bernts.wordcount.wordcountlib.WordFrequencyAnalyzer;
import nl.bernts.wordcount.wordcountlib.WordFrequencyAnalyzerImpl;
import org.junit.Assert;
import org.junit.Test;

public class WordFrequencyAnalyzerImplCalcHighestFreqTest {
    private WordFrequencyAnalyzer instance = new WordFrequencyAnalyzerImpl();
    private String inputText;
    private int expectedFrequency;

    @Test
    public void testNoWords() {
        inputText = "";
        expectedFrequency = 0;

        testResult();
    }

    @Test
    public void testOneWord() {
        inputText = "foo";
        expectedFrequency = 1;

        testResult();
    }

    @Test
    public void testOneWordSpaces() {
        inputText = " foo  ";
        expectedFrequency = 1;

        testResult();
    }

    @Test
    public void testTwoDifferentWords() {
        inputText = "foo bar";
        expectedFrequency = 1;

        testResult();
    }

    @Test
    public void testTwoWords() {
        inputText = "foo foo";
        expectedFrequency = 2;

        testResult();
    }

    @Test
    public void testTwoWordsCaps() {
        inputText = "fOo Foo";
        expectedFrequency = 2;

        testResult();
    }

    @Test
    public void testTwoWordsComma() {
        inputText = "foo,foo";
        expectedFrequency = 2;

        testResult();
    }

    private void testResult() {
        int output = instance.calculateHighestFrequency(inputText);
        Assert.assertEquals(expectedFrequency, output);
    }
}