package nl.bernts.wordcount;

import nl.bernts.wordcount.wordcountlib.WordFrequency;
import nl.bernts.wordcount.wordcountlib.WordFrequencyAnalyzer;
import nl.bernts.wordcount.wordcountlib.WordFrequencyAnalyzerImpl;
import nl.bernts.wordcount.wordcountlib.WordFrequencyImpl;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class WordFrequencyAnalyzerImplCalcMostFreqWords {
    private WordFrequencyAnalyzer instance = new WordFrequencyAnalyzerImpl();
    private String inputText;
    private int inputN;
    private List<WordFrequency> expectedWordFreqs;

    @Test
    public void testEmptyString() {
        inputText = "";
        inputN = 0;
        expectedWordFreqs = Collections.emptyList();

        testResult();
    }

    @Test
    public void testOneWordN0() {
        inputText = "foo";
        inputN = 0;
        expectedWordFreqs = Collections.emptyList();

        testResult();
    }

    @Test
    public void testOneWordN1() {
        inputText = "foo";
        inputN = 1;

        setExpectedFreqs(
                wordFreq("foo",1)
        );

        testResult();
    }

    @Test
    public void testTwoWordsN1() {
        inputText = "foo bar";
        inputN = 1;

        setExpectedFreqs(
                wordFreq("bar", 1),
                wordFreq("foo", 1)
        );

        testResult();
    }

    @Test
    public void testWordInTextTwiceWithOtherWordN1() {
        inputText = "foo foo bar";
        inputN = 1;

        setExpectedFreqs(
                wordFreq("foo", 2)
        );

        testResult();
    }

    @Test
    public void testWordInTextTwiceWithOtherWordN2() {
        inputText = "foo foo bar";
        inputN = 2;

        setExpectedFreqs(
                wordFreq("foo", 2),
                wordFreq("bar", 1)
        );

        testResult();
    }

    @Test
    public void testWordInTextThreeTimesTwiceAndOnceN2() {
        inputText = "foo foo foo bar bar test";
        inputN = 2;

        setExpectedFreqs(
                wordFreq("foo", 3),
                wordFreq("bar", 2)
        );

        testResult();
    }

    private void testResult() {
        List<WordFrequency> output = instance.calculateMostFrequentNWords(inputText, inputN);
        Assert.assertEquals(expectedWordFreqs, output);
    }

    private void setExpectedFreqs(WordFrequency... wordFreqs) {
        expectedWordFreqs = Arrays.asList(wordFreqs);
    }

    private WordFrequency wordFreq(String word, int n) {
        return new WordFrequencyImpl(word, n);
    }

}
