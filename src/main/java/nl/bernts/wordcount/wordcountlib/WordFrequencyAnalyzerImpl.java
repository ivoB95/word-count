package nl.bernts.wordcount.wordcountlib;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class WordFrequencyAnalyzerImpl implements WordFrequencyAnalyzer {
    private Pattern wordRegex = Pattern.compile("[a-zA-Z]+");
    private Pattern wholeWordRegex = Pattern.compile("^[a-zA-Z]+$");

    @Override
    public int calculateHighestFrequency(String text) {
        List<String> words = getWordsFromText(text);
        Map<String, Integer> frequenciesMap = getFrequencies(words);

        return getMaxFromFrequenciesMap(frequenciesMap);
    }

    @Override
    public int calculateFrequencyForWord(String text, String word) {
        if (!checkWordFormat(word)) {
            throw new RuntimeException("word is incorrectly formatted.");
        }

        List<String> words = getWordsFromText(text);
        Map<String, Integer> frequenciesMap = getFrequencies(words);

        String wordLowerCase = word.toLowerCase();

        return frequenciesMap.getOrDefault(wordLowerCase, 0);
    }

    private boolean checkWordFormat(String word) {
        Matcher matcher = wholeWordRegex.matcher(word);

        return matcher.matches();
    }


    @Override
    public List<WordFrequency> calculateMostFrequentNWords(String text, int n) {
        List<String> words = getWordsFromText(text);
        Map<String, Integer> frequenciesMap = getFrequencies(words);
        List<WordFrequency> wordFrequencies = frequenciesMapToWordFrequencyList(frequenciesMap);
        sortWordFrequncyList(wordFrequencies);

        return takeNWords(wordFrequencies, n);
    }


    private List<WordFrequency> frequenciesMapToWordFrequencyList(Map<String, Integer> frequenciesMap) {
        return frequenciesMap.entrySet()
                .stream()
                .map(entry -> new WordFrequencyImpl(entry.getKey(), entry.getValue()))
                .collect(Collectors.toList());
    }

    private void sortWordFrequncyList(List<WordFrequency> wordFrequencies) {
        wordFrequencies.sort((WordFrequency wf1, WordFrequency wf2) -> {
            if (wf1.getFrequency() > wf2.getFrequency()) {
                return -1;
            } else if (wf1.getFrequency() < wf2.getFrequency()) {
                return 1;
            } else {
                return wf1.getWord().compareTo(wf2.getWord());
            }
        });
    }

    private List<WordFrequency> takeNWords(List<WordFrequency> sortedWordFrequencies, int n) {
        if (sortedWordFrequencies.size() == 0) {
            return sortedWordFrequencies;
        }

        int minFreq = sortedWordFrequencies.get(0).getFrequency() - n + 1;
        List<WordFrequency> result = new LinkedList<>();

        for (WordFrequency wordFreq : sortedWordFrequencies) {
            if (wordFreq.getFrequency() < minFreq) {
                break;
            }

            result.add(wordFreq);
        }

        return result;
    }

    private List<String> getWordsFromText(String text) {
        List<String> result = new LinkedList<>();

        Matcher matcher = wordRegex.matcher(text);

        while (matcher.find()) {
            result.add(matcher.group().toLowerCase());
        }

        return result;
    }

    private Map<String, Integer> getFrequencies(List<String> words) {
        Map<String, Integer> frequenciesMap = new HashMap<>();

        for (String word : words) {
            if (!frequenciesMap.containsKey(word)) {
                frequenciesMap.put(word, 1);
            } else {
                frequenciesMap.put(word, frequenciesMap.get(word) + 1);
            }
        }

        return frequenciesMap;
    }

    private int getMaxFromFrequenciesMap(Map<String, Integer> frequenciesMap) {
        if (frequenciesMap.isEmpty()) {
            return 0;
        } else {
            return Collections.max(frequenciesMap.values());
        }
    }
}
