package nl.bernts.wordcount.wordcountlib;

public interface WordFrequency {
    String getWord();

    int getFrequency();
}
