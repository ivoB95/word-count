package nl.bernts.wordcount.wordcountlib;


import jakarta.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class WordFrequencyImpl implements WordFrequency {
    private final String word;
    private final int frequency;


    public WordFrequencyImpl(String word, int frequency) {
        this.word = word;
        this.frequency = frequency;
    }

    @Override
    public String getWord() {
        return word;
    }

    @Override
    public int getFrequency() {
        return frequency;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof WordFrequencyImpl)) {
            return false;
        }

        return equals((WordFrequencyImpl) o);
    }

    public boolean equals(WordFrequencyImpl other) {
        return this.word.equals((other.word))
                && this.frequency == other.frequency;
    }
}
