package nl.bernts.wordcount;

import nl.bernts.wordcount.wordcountlib.WordFrequency;
import nl.bernts.wordcount.wordcountlib.WordFrequencyAnalyzer;
import nl.bernts.wordcount.wordcountlib.WordFrequencyAnalyzerImpl;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Application;
import java.util.List;

@Path("word_count")
@ApplicationPath("api")
public class WordCountService extends Application {

    private WordFrequencyAnalyzer wordFrequencyAnalyzer = new WordFrequencyAnalyzerImpl();

    @GET
    @Path("/calculateHighestFrequency")
    public int calculateHighestFrequency(@QueryParam("text") String text) {
        return wordFrequencyAnalyzer.calculateHighestFrequency(text);
    }

    @GET
    @Path("calculateFrequencyForWord")
    public int calculateFrequencyForWord(@QueryParam("text") String text,
                                         @QueryParam("word") String word) {
        return wordFrequencyAnalyzer.calculateFrequencyForWord(text, word);
    }

    @GET
    @Path("calculateMostFrequentNWords")
    public List<WordFrequency> calculateMostFrequentNWords(@QueryParam("text") String text,
                                                           @QueryParam("n") int n) {
        return wordFrequencyAnalyzer.calculateMostFrequentNWords(text, n);
    }

}
